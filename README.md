# cidenet

Dentro de sus procesos de Talento Humano, Cidenet S.A.S. requiere de un sistema que le permita registrar el ingreso y la salida de sus empleados, así como administrar su información. Actualmente el proceso se realiza de manera manual sobre una hoja de cálculo, lo cual funciona pero impide alimentar otros procesos para los cuales es importante esta información, así como llevar de manera óptima el registro y administración de la misma.

## Requerimientos tecnicos

Para la ejecución de este proyecto es necesario tener instalado en su maquina Windows, Linux o Mac lo siguiente.

•	Composer

•	Symfony

•	Wamp / Xamp

•	PHP 7.2 en adelante

•	MySql

•	Editor de código / Sublime, VisualStudioCode, PHPStorm

## Installation

Luego de tener instalado los requerimientos anteriores, clone el proyecto.

En este encontrara una carpeta llamada BD con un archivo query.sql, ejecute ese script para crear la base de datos llamada cidenet

Luego corra en la consola el comando composer install

Para terminar corra el comando symfony server:start

y ya podra desplegar la website de la prueba

## Notas

Si poseen problemas con la instalacion, ejecucion de la prueba por favor contactarse conmigo. 