<?php


namespace App\Repository;


use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getAllUserQueryByFilters(User $user){
        $query = $this->createQueryBuilder('u');
        if($user->getFirstName()){
            $query->andWhere('u.firstName = :firstName' );
            $query->setParameter('firstName', $user->getFirstName());
        }
        if($user->getSecondName()){
            $query->andWhere('u.secondName = :secondName' );
            $query->setParameter('secondName', $user->getSecondName());
        }
        if($user->getSurname()){
            $query->andWhere('u.surname = :surname' );
            $query->setParameter('surname', $user->getSurname());
        }
        if($user->getSecondSurname()){
            $query->andWhere('u.secondSurname = :secondSurname' );
            $query->setParameter('secondSurname', $user->getSecondSurname());
        }
        if($user->getCountry()){
            $query->andWhere('u.country = :country' );
            $query->setParameter('country', $user->getCountry());
        }
        if($user->getDocumentType() && $user->getDocument()){
            $query->andWhere('u.documentType = :documentType' );
            $query->andWhere('u.document = :document' );
            $query->setParameter('documentType', $user->getDocumentType());
            $query->setParameter('document', $user->getDocument());
        }
        if($user->getEmail()){
            $query->andWhere('u.email = :email' );
            $query->setParameter('email', $user->getEmail());
        }
        if($user->isState()){
            $query->andWhere('u.state = :state' );
            $query->setParameter('state', $user->isState());
        }
        return $query;
    }

}