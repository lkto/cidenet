<?php


namespace App\Service;


use App\Entity\Area;
use App\Entity\Country;
use App\Entity\User;
use App\Repository\AreaRepository;
use App\Repository\CountryRepository;
use App\Repository\UserRepository;

class GetInitialInfo
{
    /**
     * @var AreaRepository
     */
    private $areaRepository;
    /**
     * @var CountryRepository
     */
    private $countryRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(AreaRepository $areaRepository, CountryRepository $countryRepository, UserRepository $userRepository)
    {
        $this->areaRepository = $areaRepository;
        $this->countryRepository = $countryRepository;
        $this->userRepository = $userRepository;
    }


    /**
     * Metodo para obtener la informacion inicial a utilizar
     */
    public function getInitialInfo() {
        return [
            "areas" => $this->getAreas(),
            "countries" => $this->getCountries(),
            "users" => $this->getUsers(),
        ];
    }

    /**
     * Metodo para obtener las areas del sistema
     */
    public function getAreas() {
        /**@var $area Area*/
        $areas = $this->areaRepository->findAll();
        $data = [];
        foreach ($areas as $area) {
            $tempInfo = [
                "id" => $area->getId(),
                "name" => $area->getName()
            ];

            array_push($data, $tempInfo);
        }

        return $data;
    }

    /**
     * Metodo para obtener los paises del sistema
     */
    public function getCountries() {
        /**@var $country Country*/
        $countries = $this->countryRepository->findAll();
        $data = [];
        foreach ($countries as $country) {
            $tempInfo = [
                "id" => $country->getId(),
                "name" => $country->getName()
            ];

            array_push($data, $tempInfo);
        }
        return $data;
    }

    /**
     * Metodo para obtener los usuarios del sistema
     */
    public function getUsers() {
        /**@var $user User*/
        $users = $this->userRepository->findAll();
        $data = [];
        foreach ($users as $user) {
            $tempInfo = [
                "id" => $user->getId(),
                "email" => $user->getEmail(),
                "documentType" => $this->documentTypeName($user->getDocumentType()),
                "documentNumber" => $user->getDocument(),
                "name" => $user->getFirstName() . " " . $user->getSecondName(),
                "surname" => $user->getSurname() . " " . $user->getSecondSurname(),
                "country" => $user->getCountry(),
                "area" => $user->getArea()
            ];
            array_push($data, $tempInfo);
        }
        return $data;
    }

    public function documentTypeName($code) {
        return User::getTypeIdByCode($code);
    }

}