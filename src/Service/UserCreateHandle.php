<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserCreateHandle
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user){
        $date = new \DateTime('now');
        $user->setState(true);
//        $user->setRegistrationDate($date);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $user->setEmail($this->createEmail($user));
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function edit(User $user){
        $date = new \DateTime('now');
//        $user->setEditionDate($date);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $user->setEmail($this->createEmail($user));
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }


    /**
     * @param User $user
     * @return string
     */
    public function createEmail(User $user){
        $email = $user->getFirstName().'.'.$user->getSurname().'@'. $user->getCountry()->getDomain();
        /**@var User $existUser*/
        $existUser = $this->userRepository->findOneBy(['email' => $email]);

        if($existUser){
            if($existUser->getId() !== $user->getId()){
                $email = $user->getFirstName().'.'.$user->getSurname(). '.'. $user->getId(). '@'. $user->getCountry()->getDomain();
            }
        }

        return $email;
    }

}