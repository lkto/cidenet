<?php


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    const CC = "Cédula de Ciudadanía";
    const CE = "Cédula de Extranjería";
    const PP = "Pasaporte";
    const PE = "Permiso Especial";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private  $id;

    /**
     * @var string
     * @ORM\Column(name="first_name")
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(name="second_name", nullable=true)
     */
    private $secondName;

    /**
     * @var string
     * @ORM\Column(name="surname")
     */
    private $surname;

    /**
     * @var string
     * @ORM\Column(name="second_surname", nullable=true)
     */
    private $secondSurname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Area")
     */
    private $area;

    /**
     * @var string
     * @ORM\Column(name="document_type")
     */
    private $documentType;

    /**
     * @var string
     * @ORM\Column(name="document")
     */
    private $document;

    /**
     * @var string
     * @ORM\Column(name="email", nullable=true)
     */
    private $email;

    /**
     * @var DateTime
     * @ORM\Column(name="date_of_admission", type="datetime")
     */
    private $dateOfAdmission;

    /**
     * @var boolean
     * @ORM\Column(name="state")
     */
    private $state;

    /**
     * @var DateTime
     * @ORM\Column(name="registration_date", type="datetime")
     */
    private $registrationDate;

    /**
     * @var DateTime
     * @ORM\Column(name="edition_date", type="datetime", nullable=true)
     */
    private $editionDate;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    /**
     * @param $secondName
     */
    public function setSecondName($secondName): void
    {
        $this->secondName = $secondName;
    }

    /**
     * @return string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getSecondSurname(): ?string
    {
        return $this->secondSurname;
    }

    /**
     * @param string $secondSurname
     */
    public function setSecondSurname(string $secondSurname): void
    {
        $this->secondSurname = $secondSurname;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @return Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param Area $area
     */
    public function setArea(Area $area): void
    {
        $this->area = $area;
    }

    /**
     * @return string
     */
    public function getDocumentType(): ?string
    {
        return $this->documentType;
    }

    /**
     * @param string $documentType
     */
    public function setDocumentType(string $documentType): void
    {
        $this->documentType = $documentType;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getDateOfAdmission(): ?DateTime
    {
        return $this->dateOfAdmission;
    }

    /**
     * @param DateTime $dateOfAdmission
     */
    public function setDateOfAdmission(DateTime $dateOfAdmission): void
    {
        $this->dateOfAdmission = $dateOfAdmission;
    }

    /**
     * @return bool
     */
    public function isState(): ?bool
    {
        return $this->state;
    }

    /**
     * @param bool $state
     */
    public function setState(bool $state): void
    {
        $this->state = $state;
    }

    /**
     * @return DateTime
     */
    public function getRegistrationDate(): ?DateTime
    {
        return $this->registrationDate;
    }

    /**
     * @param DateTime $registrationDate
     */
    public function setRegistrationDate(DateTime $registrationDate): void
    {
        $this->registrationDate = $registrationDate;
    }

    /**
     * @return DateTime
     */
    public function getEditionDate(): ?DateTime
    {
        return $this->editionDate;
    }

    /**
     * @param DateTime $editionDate
     */
    public function setEditionDate(DateTime $editionDate): void
    {
        $this->editionDate = $editionDate;
    }

    /**
     * @return string
     */
    public function getDocument(): ?string
    {
        return $this->document;
    }

    /**
     * @param string $document
     */
    public function setDocument(string $document): void
    {
        $this->document = $document;
    }

    static function getTypeIdByCode($code) {
        $name = "";
        switch ($code) {
            case "CC":
                $name = self::CC;
                break;
            case "CE":
                $name = self::CE;
                break;
            case "PP":
                $name = self::PP;
                break;
            case "PE":
                $name = self::PE;
                break;
        }
        return $name;
    }

    static function getAllDocumentType() {
        return [
            self::CC => 'CC',
            self::CE => 'CE',
            self::PE => 'PE',
            self::PP => 'PP',
        ];
    }

    public function createUser(array $data, Country $country, Area $area)
    {

    }
}