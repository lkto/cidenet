<?php


namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\GetInitialInfo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rest")
 * @param Request $request
 * @return Response
 */
class RestController extends AbstractController
{
    /**
     * @Route("/user_exist", name="user_exist")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Response
     */
    public function index(Request $request, UserRepository $userRepository)
    {
        $documentType = $request->get('documentType');
        $document = $request->get('document');

        $user = $userRepository->findOneBy(['documentType' => $documentType, 'document' => $document]);

        $exist = false;
        $id = null;

        if ($user){
            $exist = true;
            $id = $user->getId();
        }

        $data = [
            'exist' => $exist,
            'id' => $id
        ];
        $response = new Response();
        $response->setContent(json_encode($data));

        return $response;
    }

    /**
     * @Route("/initial_info", name="initial_info")
     * @param GetInitialInfo $getInitialInfo
     * @return Response
     */
    public function getInitialInfo(GetInitialInfo $getInitialInfo)
    {
        $data = $getInitialInfo->getInitialInfo();
        $response = new Response();
        $response->setContent(json_encode($data));
        return $response;
    }

}