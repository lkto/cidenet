<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\Type\UserFormType;
use App\Service\UserCreateHandle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/user/create", name="create_user")
     * @param Request $request
     * @param UserCreateHandle $userCreateHandle
     * @return Response
     */
    public function index(Request $request, UserCreateHandle $userCreateHandle)
    {
        $date = new \DateTime('now');
        $user = new User();
        $user->setRegistrationDate($date);
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $newUser = $userCreateHandle->create($user);
            $this->addFlash('success', 'El usuario con email '.$newUser->getEmail().' a sido registrado con exito');
            return $this->redirectToRoute('home');
        }



        return $this->render('User/create.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'edit' => false,
            'date' => $date->format('d-m-Y H:m:s')
        ]);
    }

    /**
     * @Route("/user/edit/{user}", name="edit_user")
     * @param User $user
     * @param Request $request
     * @param UserCreateHandle $userCreateHandle
     * @return Response
     */
    public function edit(User $user, Request $request, UserCreateHandle $userCreateHandle)
    {
        $date = new \DateTime('now');
        $user->setEditionDate($date);
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $newUser = $userCreateHandle->edit($user);
            $this->addFlash('success', 'El usuario con email '.$newUser->getEmail().' a sido modificado con exito');
            return $this->redirectToRoute('home');
        }



        return $this->render('User/create.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'edit' => true,
            'date' => $date->format('d-m-Y H:m:s')
        ]);
    }

    /**
     * @Route("/user/delete/{user}", name="delete_user")
     * @param User $user
     * @return Response
     */
    public function delete(User $user)
    {
        $email = $user->getEmail();

        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('delete', 'El usuario con email '.$email.' a sido eliminado con exito');
        return $this->redirectToRoute('home');
    }

}