<?php


namespace App\Controller;


//use App\Entity\User;
use App\Entity\User;
use App\Form\Type\UserFilterType;
use App\Repository\UserRepository;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, UserRepository $userRepository, PaginatorInterface $paginator)
    {
        $user = new User();
        $form = $this->createForm(UserFilterType::class, $user);
        $page = 1;
        $filters = $user;
        if($request->query->get("page")){
            $page = $request->query->get("page");
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filters = $form->getData();
        }

        $pagination = $paginator->paginate(
            $userRepository->getAllUserQueryByFilters($filters),
            $page,
            1
        );

        return $this->render('Home/index.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
        ]);
    }

}