<?php


namespace App\Form\Type;


use App\Entity\Area;
use App\Entity\Country;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('firstName', TextType::class, [
            'label' => 'Primer Nombre',
            'attr' => [
                'class' => 'form-control',
                'maxlength' => '20',
                'onkeypress' => 'return validateName(event)'
            ]

        ]);

        $builder->add('secondName', TextType::class, [
            'label' => 'Otros Nombres',
            'attr' => [
                'class' => 'form-control',
                'maxlength' => '50',
                'onkeypress' => 'return validateOtherName(event)'
            ]
        ]);

        $builder->add('surname', TextType::class, [
            'label' => 'Primer Apellido',
            'attr' => [
                'class' => 'form-control',
                'maxlength' => '20',
                'onkeypress' => 'return validateName(event)'
            ]
        ]);

        $builder->add('secondSurname', TextType::class, [
            'label' => 'Segundo Apellido',
            'attr' => [
                'class' => 'form-control',
                'maxlength' => '20',
                'onkeypress' => 'return validateName(event)'
            ]
        ]);

        $builder->add('country', EntityType::class, [
            'class' => Country::class,
            'choice_label' => 'name',
            'label' => 'Pais del empleado',
            'attr' => [
                'class' => 'form-control',
                'style' => 'background: #202940'
            ]
        ]);

        $builder->add('documentType', ChoiceType::class, [
            'choices'  => User::getAllDocumentType(),
            'label' => 'Tipo de Identificación',
            'attr' => [
                'class' => 'form-control',
                'style' => 'background: #202940'
            ]
        ]);

        $builder->add('document', TextType::class, [
            'label' => 'Número de Identificación',
            'attr' => [
                'class' => 'form-control',
                'onkeypress' => 'return validateDocument(event)'
            ]
        ]);

        $builder->add('dateOfAdmission', DateType::class, [
            'label' => 'Fecha de ingreso',
            'attr' => [
                'class' => 'form-control js-datepicker',
                'data-provide' => 'datetimepicker',
                'onchange' => 'changeDate(event)'
            ],
            'widget' => 'single_text',
        ]);

        $builder->add('area', EntityType::class, [
            'class' => Area::class,
            'choice_label' => 'name',
            'label' => 'Área',
            'attr' => [
                'class' => 'form-control',
                'style' => 'background: #202940'
            ]
        ]);

    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}